// This document contains our app feature in retrieving and manipulating our database

const Task = require('../models/task.js');

module.exports.getAllTask = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({ // from 'Task' model
	/*
		name :{
			firstName: requestBody.firstName,
			lastName: requestBody.lastName
		}
	*/
		name: requestBody.name // only the name is required
	});

	return newTask.save().then((task, error) => {
		if (error) {
			console.log(error);
			return false; 
		}
		else {
			return task; // from parameter
		}
	})
}

// 'taskId' parameter will serve as storage of id from our url/link
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if (err) {
			console.log(err);
			return false;
		}
		else {
			return removedTask;
		}
	})
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if (error) {
			console.log(error);
			return false;
		}
		result.name = newContent.name;


		return result.save().then((updateTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return false;
			}
			else {
				return updateTask;
			}
		})
	})
}

//findone -body
// whole doc of task

/*
	1. Create a controller function for retrieving a specific task.
	2. Create a route 
	3. Return the result back to the client/Postman.
*/

module.exports.retrieveTask = (taskId) => {
	return Task.findById(taskId).then((specificTask, err) => {
		if (err) {
			console.log(err);
			return false;
		}
		else {
			return specificTask;
		}
	})
}

/*
	5. Create a controller function for changing the status of a task to "complete".
	6. Create a route
	7. Return the result back to the client/Postman.
	8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
*/

module.exports.updateStatus = (taskId, newStatus) => {
	return Task.findById(taskId,).then((result, err) => {
		if (err) {
			console.log(err);
			return false;
		}

		result.status = newStatus.status;

		return result.save().then((updateStatus, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return false;
			}
			else {
				return updateStatus;
			}
		});
	});
}
