// This document contains all the endpoints for our application (also http methods)

const express = require('express');
const router = express.Router();
const taskController = require('../controllers/taskController.js');

// GET do not submit data
router.get('/viewTasks', (req, res) => {
	// Invokes the 'getAlltasks' function fro the 'taskController.js' file and sends the result back to the client/Postman
								// returns result from our controller
	taskController.getAllTask().then(result => res.send(result));
});

// POST requires input to put into the database
router.post('/addNewTask', (req, res) => {
								// gets input from this data source
	taskController.createTask(req.body).then(result => res.send(result));
});

// DELETE items using url
router.delete('/deleteTask/:id', (req, res) => {
	taskController.deleteTask(req.params.id).then(result => res.send(result));
});

// PUT method - update tasks
router.put('/updateTask/:id', (req, res) => {
	// req.params.id - will be the basis of what document we will update
	// req.body - where to put the new documents/contents
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
});
/*
	4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
*/

router.get('/:id', (req, res) => {
	taskController.retrieveTask(req.params.id).then(result => res.send(result));
});

/*
	8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
*/

router.put('/:id/complete', (req, res) => {
	taskController.updateStatus(req.params.id, req.body).then(result => res.send(result));
});

module.exports = router;